<?php

namespace app\controllers;

use app\models\clientes;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ClientesController implements the CRUD actions for clientes model.
 */
class ClientesController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all clientes models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => clientes::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'dni' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single clientes model.
     * @param string $dni Dni
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($dni)
    {
        return $this->render('view', [
            'model' => $this->findModel($dni),
        ]);
    }

    /**
     * Creates a new clientes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new clientes();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'dni' => $model->dni]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing clientes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $dni Dni
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($dni)
    {
        $model = $this->findModel($dni);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'dni' => $model->dni]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing clientes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $dni Dni
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($dni)
    {
        $this->findModel($dni)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the clientes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $dni Dni
     * @return clientes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($dni)
    {
        if (($model = clientes::findOne(['dni' => $dni])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
