<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "movimientos".
 *
 * @property int $numero
 * @property string $tipo
 * @property int|null $cantidad
 * @property string $dni
 * @property int $id
 *
 * @property Clientes $dni0
 * @property Empleados $id0
 */
class Movimientos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'movimientos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['numero', 'tipo', 'dni', 'id'], 'required'],
            [['numero', 'cantidad', 'id'], 'integer'],
            [['tipo'], 'string', 'max' => 30],
            [['dni'], 'string', 'max' => 9],
            [['id'], 'unique'],
            [['dni'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['dni' => 'dni']],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => Empleados::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'numero' => 'Numero',
            'tipo' => 'Tipo',
            'cantidad' => 'Cantidad',
            'dni' => 'Dni',
            'id' => 'ID',
        ];
    }

    /**
     * Gets query for [[Dni0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDni0()
    {
        return $this->hasOne(Clientes::className(), ['dni' => 'dni']);
    }

    /**
     * Gets query for [[Id0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(Empleados::className(), ['id' => 'id']);
    }
}
